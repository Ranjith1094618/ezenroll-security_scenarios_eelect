@Test1
Feature: Authentication Testing

  #@EncryptedChannel
 # Scenario: Credentials transport over an encrypted channel
   # Given Employee opens eElect Application in Browser
   # When   Employee enters Username and Password in Login Page
    #Then  Verify the Username and Password of eElect user is passed in an encrypted manner


  @EnhancedSecurity
  Scenario: Testing for default credentials
    Given Employee opens AutoBene application in browser
    When Employee enters login details in Login Page
    And Employee enters Enhance Security App#
    When  Enable Enhanced Security in AutoBene
    Then  Try to Login eElect through EID and PIN

  @HTTP
  Scenario: HTTP Strict Transport Security
    Given Validate Testenvironment URL


  @LockOut
  Scenario: Testing for Weak lock out mechanism
    Given Employee opens eElect Application in Browser
    When Validate the eElect application locks out after #mins of idleness

 # @Remember
  #Scenario: Testing for Vulnerable remember password
   # Given Employee opens eElect Application in Browser
   # When  Employee enters Username and Password in Login Page
   # And   Verify browser should auto populate the password of your login.

  @Cache
  Scenario: Testing for Browser cache weakness
    Given Employee opens eElect Application in Browser
    When  Employee enters Username and Password in Login Page
    Then Verify Cache Weakness












