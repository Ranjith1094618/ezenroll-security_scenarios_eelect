var data1 = require('../../test_resources/Security_Data');
//var action1 = require('../../utils/CommonUtilityEZEnroll');
var objects = require('../../repository/eElectPage');
var action = require('./../../Keywords');
//var clipboard = require('../../node_modules/copy-paste');
var clipboard = require('copy-paste');
var AutoBenePage, eElectSection, Pages;


var execEnv = data1["TestingEnvironment"];


function initializePageObjects(browser, callback) {

    Pages = browser.page.eElectPage();
    AutoBenePage = Pages.section.AutoBenePage;
    eElectSection = Pages.section.eElectSection;
    callback();
}


module.exports = function () {
    //Feature : Authorization Testing
    // TagName : eElect
    //Scenario : Testing for Privilege escalation

    this.Given(/^Employee opens eElect Application in Browser$/, function () {
        var eElectURL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {
            eElectURL = data1.urls.eElect[execEnv].vaildUrl;

            browser.maximizeWindow()
            // .deleteCookies()
                .url(eElectURL);
            browser.timeoutsImplicitWait(data1.longWait);
            initializePageObjects(browser, function () {
                eElectSection.waitForElementVisible('@eElectSearch', data1.shortWait);
                eElectSection.setValue('@eElectSearch', data1.users.eElect[execEnv].ApplicationID);
                eElectSection.click('@eElectContinue', function () {
                });
            })
        }
    });

    this.Given(/^Employee opens eElect Non ESApplication in Browser$/, function () {
        var eElectURL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {
            eElectURL = data1.urls.eElect[execEnv].vaildUrl;

            browser.maximizeWindow()
            // .deleteCookies()
                .url(eElectURL);
            browser.timeoutsImplicitWait(data1.longWait);
            initializePageObjects(browser, function () {
                eElectSection.waitForElementVisible('@eElectSearch', data1.shortWait);
                eElectSection.setValue('@eElectSearch', data1.users.eElect[execEnv].Es_AppID);
                eElectSection.click('@eElectContinue', function () {
                });
            })
        }
    });

    this.When(/^Employee enters Username and Password in Login Page$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        eElectSection.click('@eElect_Username');
        browser.getCookie('@eElect_Username', function (result) {
            console.log(result);
        });
        eElectSection.setValue('@eElect_Username', data1.users.eElect[execEnv].username);                 // Enter Username
        eElectSection.setValue('@eElect_Password', data1.users.eElect[execEnv].password);                      // Enter password
        browser.pause(2000);
        eElectSection.click('@eElect_Login', function () {
        });

    });
    this.When(/^Employee try to edit eElect Information$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        eElectSection.waitForElementNotPresent('@AccSettings', data1.shortWait);
        browser.pause(data1.shortWait);
    });


    //Feature : Authorization Testing
    //TagName : AutoBene
    //Testing for Bypassing Authorization Schema
    this.Given(/^Employee opens AutoBene application in browser$/, function () {
        var URL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {
            URL = data1.urls.AUTO_BENE[execEnv].vaildUrl;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .url(URL);
                browser.timeoutsImplicitWait(data1.longWait);
                browser.frame(1, function () {
                    AutoBenePage.click('@AutoBeneLink')
                });
            })
        }
    });


    this.Given(/^Employee enters login details in Login Page$/, function () {
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.setValue('@LoginID', data1.users.AUTO_BENE[execEnv].username, function () {
            });
            AutoBenePage.setValue('@Pwd', data1.users.AUTO_BENE[execEnv].password);
        });
        AutoBenePage.click('@LoginButton', function () {
            browser.pause(data1.shortWait);
        });
    });


    this.When(/^Employee enters AppID$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);

            AutoBenePage.setValue('@AppIDSearch', data1.users.AUTO_BENE[execEnv].ApplicationID, function () {
                AutoBenePage.click('@GoBtn', function () {
                });
            });
            browser.pause(data1.shortWait);
        })
    });
    this.When(/^Employee selects BeneDetails option for Application used for in Global Parameters Page do not select eElect option$/, function () {
        browser.timeoutsImplicitWait(2000);
        AutoBenePage.click('@Change_Menu', function () {
        });
        browser.pause(data1.shortWait);
        AutoBenePage.click('@GlobalParameters', function () {
        });
        browser.pause(data1.shortWait);
        AutoBenePage.click('@BeneRadiobtn', function () {
        });
        browser.pause(3000);
        AutoBenePage.click('@GlobalContinueBtn', function () {
        });
        browser.pause(data1.shortWait);
        AutoBenePage.click('@GlobalContinueBtn2', function () {
        });
    });


    //Feature : Authentication Testing
    //TagName :
    //Testing for Bypassing Authorization Schema
    this.Given(/^Validate Testenvironment URL$/, function () {
        var invalidURL;
        browser = this;
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "TEST1" || execEnv.toUpperCase() == "QA" || execEnv.toUpperCase() == "STAGING" || execEnv.toUpperCase() == "PROD") {

            invalidURL = data1.urls.AUTO_BENE[execEnv].invalidUrl;
            var validURL = data1.urls.AUTO_BENE[execEnv].vaildUrl;
            console.log(invalidURL);
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .pause(2000)
                    .url(invalidURL);
                browser.url(function (response) {
                    console.log();
                    if (response.value == invalidURL) {
                        browser.assert.equal(response.value, validURL);
                        console.log("Invalid URL");
                    }
                })
            })
        }
    });

    //Feature : Authentication Testing
    //TagName : HTTP
    //Testing for Testing for default credentials

    this.When(/^Employee enters Enhance Security App#$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.setValue('@AppIDSearch', data1.users.AUTO_BENE[execEnv].Es_AppID)
            AutoBenePage.click('@GoBtn')
        })
    });

    this.When(/^Enable Enhanced Security in AutoBene$/, function () {
        browser.timeoutsImplicitWait(2000);
        browser.frame(1, function () {
            browser.pause(data1.shortWait);
            AutoBenePage.click('@Change_Menu', function () {
            });
            browser.pause(2000);
            AutoBenePage.click('@SecurityRequirement', function () {
            });

            //  browser.verify.visible('input[name="ckbUsePassSecurity"]', 'The checkbox named vote is visible')
            //      .waitForElementVisible('body', 1000)
            //      .element('name', 'ckbUsePassSecurity', function (response) {
            //          browser.elementIdSelected(response.value.ELEMENT, function (result) {
            //              browser.verify.ok(result.value); //'Checkbox is selected');
            //              console.log(result.value);
            //              browser.pause(2000);
            browser.pause(data1.shortWait);
            AutoBenePage.click('@Continue1', function () {
            });
            browser.pause(data1.shortWait);
            AutoBenePage.click('@eElectLink', function () {
            });
            browser.pause(data1.shortWait);
            browser.windowHandles(function (result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
                browser.pause(data1.shortWait);
                //eElectSection.setValue('@eElectSearch', data1.users.eElect[execEnv].Es_AppID);
                //eElectSection.click('@GoContinue', function () {
            });
        });

    });


    this.When(/^Try to Login eElect through EID and PIN$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        eElectSection.setValue('@eElect_eid', data1.users.eElect[execEnv].EID, function () {

        });        //Enter EID given in Auto bene section
        browser.pause(data1.shortWait);
        eElectSection.setValue('@eElect_Pwd', data1.users.eElect[execEnv].PIN);             // Enter PIN given in Auto bene section
        browser.pause(data1.shortWait);
        eElectSection.click('@eElect_Login', function () {
        });
        browser.pause(data1.shortWait);
        /*eElectSection.getText('@createnew_header', function (res) {
            var actual = JSON.stringify(res.value);
            eElectSection.assert.equal(actual, "Create New Account")
        })*/

    });
    this.When(/^Login with EID and PIN$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        eElectSection.setValue('@eElect_Username', data1.users.eElect[execEnv].EID, function () {

        });        //Enter EID given in Auto bene section
        browser.pause(data1.shortWait);
        eElectSection.setValue('@eElect_Password', data1.users.eElect[execEnv].PIN);             // Enter PIN given in Auto bene section
        browser.pause(data1.shortWait);
        eElectSection.click('@eElect_Login', function () {
        });
        browser.pause(data1.shortWait);
        /*eElectSection.getText('@createnew_header', function (res) {
            var actual = JSON.stringify(res.value);
            eElectSection.assert.equal(actual, "Create New Account")
        })*/

    });


    //Feature : Error Handling
    //TagName : Error Code
    //Testing for Error Code
    this.When(/^Employee enters incorrect Username and Password$/, function () {
        browser.timeoutsImplicitWait(data1.shortWait);
        eElectSection.setValue('@eElect_Username', data1.users.eElect[execEnv].invalidUsername);         // Enter invalid Username
        eElectSection.setValue('@eElect_Password', data1.users.eElect[execEnv].invalidpassword);             // Enter invalid password
        browser.pause(data1.shortWait);
        eElectSection.click('@eElect_Login', function () {
        });
        browser.pause(data1.shortWait);
    });

    this.When(/^Validate Error message$/, function () {
        //browser.frame(0, function () {
            eElectSection.getText('@Errormsg1', function (response) {
                var expected = '"\n"' +
                    '"We are sorry, the Password does not match the system security for the Username you entered. Please check the Username and Password combination and enter them again. Note: After 3 failed attempts your account will be locked.(-1)\t\t\t\t\t\t"';
                console.log("Excepted: " + expected);
                console.log("Actual: " + response.value);
                browser.assert.equal(response.value, expected);
                if (response.value != expected) {
                    console.log("Text Content mismatch: ");
                }
            });

        //})
    });
    this.Then(/^Validate the eElect application locks out after #mins of idleness$/, function () {
        browser.pause(600000);
        //browser.refresh();
        eElectSection.getText('@Errorlockout', function (text) {
            var Expected_Text = "Your session is about to expire!";
            browser.assert.equal(text, Expected_Text);
        })
    });

    this.When(/^Verify browser should auto populate the password of your login.$/, function () {
        eElectSection.click('@LogOff', function () {
        });
        //   browser.pause(data1.shortWait);
        //   BeneDetailsSection.getText('@Bene_Username', function (text) {
        //       console.log(text)
        //   });
        //   BeneDetailsSection.getText('@Bene_Pwd', function (text) {
        //       console.log(text)
        //   })
    });
    this.When(/^Verify Cache Weakness$/, function () {

        eElectSection.click('@LogOff', function () {
        });
        browser.pause(data1.shortWait);
        eElectSection.click('@ConfirmLogoff', function () {

        });
        browser.back();
        browser.pause(data1.shortWait);
        /*eElectSection.waitForElementVisible('@eElect_Username', data1.shortWait);
        eElectSection.getText('@eElect_Username', function (text) {
            var text1 = JSON.stringify(text);
            var arr = text1.split(',');
            var value = arr[2];
            var arr2 = value.split(':');
            var actual_value = arr2[1];
            if (actual_value === "" && typeof variable === "string") ;
            //console.log(actual_value);
            // var expected_value = "";
            // browser.assert.equal(actual_value, expected_value);
        });
        eElectSection.getText('@eElect_Password', function (text) {
            console.log(text);*/
        eElectSection.getText('@eElect_Expire', function (res) {
            var actual = JSON.stringify(res.value);
            console.log(actual);
            eElectSection.assert.equal(actual, '"Your session has expired! Please login and try again."')
        })
    });

    this.When(/^Test Upload of Unexpected File Types$/, function () {
        eElectSection.click('@Document_icon', function () {
        });
        browser.setValue('input[type=\'file\']', require('path').resolve("C:\\Program Files\\Git\\git-bash.exe"));
        browser.pause(data1.shortWait);


    })
}


