﻿//var Objects = require(__dirname + '/../../objects/ConncectureDrx_locators.js');
//var robot = require('robot-js');
var Objects;
var clipboard = require('copy-paste');
//var data=require('./GlobalTestData');
var page,  loginpage, homePageConnectureDrx;

//var robot123 = require('robotjs');
//ar xlsx = require('node-xlsx');
//var _ = require('underscore');
//var excel_compare = require("./samp");
//const osHomedir = require('os-homedir');
//var path = osHomedir();
//var glob = require("glob")
//var fs=require('fs');
//var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
module.exports = function(){
	
	this.Given(/^Link navigation validation - "([^"]*)" "([^"]*)"$/, function (link, landingPage) {
	//	browser = this;
		performClick(link, function () {
			isDisplayed(landingPage, function () {
				browser.back();
			});
		});
	});
	
	this.When(/^User Clicks on "([^"]*)" Link or Button$/, function(browser,locator) {
		performClick(locator, function () {
			browser.pause(data.shortpause);
		});
	});
	
	this.Then(/^"([^"]*)" page or message should be displayed$/, function(browser,locator){
		isDisplayed(locator, function () {
			browser.pause(data.shortpause)
		});
	});
	
	this.When(/^Enter "([^"]*)" field with "([^"]*)" value$/, function (locator, value) {
		setText(locator, value)
	});
	
	this.When(/^Select "([^"]*)" in "([^"]*)" dropdown$/, function (browser,value, locator) {
		//selectDropdown(locator, value, function (callback) {})
		selectWithVisibleText(locator, value, function (selected) {
			browser.pause(data.shortpause);
		});
	});
	
	this.When(/^Randomly select in "([^"]*)" dropdown$/, function (locator, callback) {
		selectRandomly(locator, function (selectedValue) {
			callback(selectedValue);
		});
	});
	
	this.When(/^Select "([^"]*)" in "([^"]*)" dropdown as user$/, function (browser,value, locator) {
		selectDropdownUI(locator, value);
	});
	
	this.Then(/^"([^"]*)" alert should be displayed$/, function(txt){
		verifyAlert(txt, this);
	});
	
	this.Then(/^"([^"]*)" page or message should not be displayed$/, function (locator) {
		isNotDisplayed(locator, function () {
			browser.pause(data.shortpause)
		});
	});
	
	this.Then(/^"([^"]*)" page or message should not be present$/, function (locator) {
		isNotPresent(locator, function () {
			browser.pause(data.shortpause)
		});
	});
	
	this.Then(/^"([^"]*)" field should be in disabled state$/, function (locator) {
		isDisabled(locator, function () {
			browser.pause(data.shortpause)
		});
	});
	
	this.Given(/^Once the user navigate to Money page$/, function () {
		//browser = this;
		performClick("HomePage|linkMenu1", function () {
			performClick("HomePage|linkMoneyMenuSelect", function () {
				isDisplayed("PensionPage|labelPensionSandboxPage", function () {
				});
			});
		});
	});
	
	this.Given(/^Once the user navigate to Profile page$/, function () {
		//browser = this;
		var objLocation = Objects.sections.ProfilePage.elements.userProfileDDMenu.selector;
		var locateStrategy = Objects.sections.ProfilePage.elements.userProfileDDMenu.locateStrategy;
		
		if (locateStrategy == 'xpath') {
			browser.useXpath().moveToElement(objLocation, 0, 0);
		} else {
			browser.useCss().moveToElement(objLocation, 0, 0);
		}
		browser.mouseButtonDown(0);
		performClick("ProfilePage|userProfileLink", function () {
			isDisplayed("ProfilePage|labelProfilePageAboutMe", function () {
			});
		});
		browser.mouseButtonUp(0);
		
	});
	
	this.Given(/^Enter in "([^"]*)" from data sheet "([^"]*)"$/, function (locator, dataObject) {
		var arr = dataObject.split("|");
		var obj = arr[0];
		var field = arr[1];
		if ((data.TestingEnvironment).toUpperCase() == "QA") {
			var value = data.usersQA_V2[obj][field];
		} else {
			var value = data.usersUAT_V2[obj][field];
		}
		console.log(value);
		setText(locator, value);
	});
	
	this.Given(/^Verify "([^"]*)" field displays "([^"]*)" value$/, function (locator, expectedValue) {
		var arr = locator.split("|");
		if (arr[1].includes("input")) {
			verifyInputBoxText(locator, expectedValue);
		} else {
			verifyText(locator, expectedValue);
		}
	});
	
};

/*   ------------ (Reusable) Commonly Used Functions Below------------- */
var initializePageObjects= function(client, callback) {
	var PageRepo;
	//browser = client;
	//(data.PageSet.PageRepos).trim()
	console.log((data.PageSet.PageRepos).trim())
	if((data.PageSet.PageRepos).trim() == "AdminToolkit")
	{
		PageRepo=browser.page.AdminToolkitPages();
		page = PageRepo.section;
		Objects=require('./mercer_health_AdminToolkit/objects/AdminToolkitPages');
		console.log('I m here in admin toolkit!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "Afweb")
	{
		PageRepo=browser.page.Afweb_locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_afweb/objects/Afweb_locators');
		console.log('I m here in afweb!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "ConnectureDRX")
	{
		PageRepo=browser.page.ConncectureDrx_locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_ConnectureDrx/objects/ConncectureDrx_locators');
		console.log('I m here in connecture!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "ConsumerPortal")
	{
		PageRepo=browser.page.ConsulatantPortalPages();
		page = PageRepo.section;
		console.log('I m here!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "DocProd")
	{
		PageRepo=browser.page.DocProd_Locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_DocProd/objects/DocProd_locators');
		console.log('I m here in DocProd!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "ESR")
	{
		PageRepo=browser.page.ESR_Locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_ESR/objects/ESR_Locators');
		//console.log('I m here in ESR!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "GetInsured")
	{
		PageRepo=browser.page.Get_InsuredPages();
		page = PageRepo.section;
		Objects=require('./mercer_health_GetInsured/objects/Get_InsuredPages');
		console.log('I m here in Get Insured!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "HRAdmin")
	{
		PageRepo=browser.page.HRAdminPortalPages();
		page = PageRepo.section;
		Objects=require('./mercer_health_HRAdminPortal/objects/HRAdminPortalPages');
		console.log('I m here in HRAdmin!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "HRAXeos")
	{
		PageRepo=browser.page.HRA_XEOS_Locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_HRAXEOS/objects/HRA_XEOS_Locators');
		console.log('I m here in HRA Xeos!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "IBC")
	{
		PageRepo=browser.page.IBC_locators();
		page = PageRepo.section;
		Objects=require('./mercer_health_IBC/objects/IBC_locators');
		console.log('I m here in IBC!! '+ page);
		callback();
	}
	else if((data.PageSet.PageRepos).trim() == "VIPAdmin")
	{
		PageRepo=browser.page.VIPAdminPages();
		page = PageRepo.section;
		Objects=require('./mercer_health_VIP_Admin/objects/VIPAdminPages');
		console.log('I m here in VIP admin!! '+ page);
		callback();
	}
};
var scroll = function (locator, callback) {
	var pageAndObject = locator.split("|");
	var pg = pageAndObject[0];
	var obj = '@'+pageAndObject[1];
	
	page[pg].getLocation(obj, function (position) {
		browser.execute(function (x, y) {
			window.scrollTo(x - 300, y - 300);
			return true
		}, [position.value.x, position.value.y]);
		callback();
	});
};

var verifyAlert = function (txt, browser) {
	browser.getAlertText(function (alertText) {
		browser.assert.equal(alertText.value, txt)
	});
	browser.acceptAlert();
};

var isDisplayed = function (locator, callback) {
	var pageAndObject = locator.split("|");
	var pg = pageAndObject[0];
	var object = '@'+pageAndObject[1];
	page[pg].waitForElementVisible(object, 25000, function () {
		callback();
	});
};

var isNotDisplayed = function (locator, callback) {
	browser.pause(data.averagepause);
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@' + pageAndObject[1];
	page[mpage].waitForElementNotVisible(object, data.averagepause, function () {
		callback()
	})
};

/*
var elementDisplayedStatus = function (locator, callback) {
	var pageObject = locator.split("|");
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var status;
	
	if (locateStrategy == 'xpath') locateStrategy = 'xpath';
	else locateStrategy = 'css selector';
	browser.pause(data.averagepause)
	browser.element(locateStrategy, object, function (obj) {
		browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
			console.log(object_displayed_status.status)
			if (object_displayed_status.status == 0) status = true;
			else status = false;
			callback(status);
		});
	});
};
*/

/*
var elementDisplayedStatus = function (locator, callback) {
	if (locator.includes("|")) {
		var pageObject = locator.split("|");
		var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
		var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
		var status;
		if (locateStrategy == 'xpath') locateStrategy = 'xpath';
		else locateStrategy = 'css selector';
		browser.pause(data.shortWait);
		browser.element(locateStrategy, object, function (obj) {
			browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
				console.log(object_displayed_status.state);
				if (object_displayed_status.state == 'success') callback(true);
				else callback(false);
			});
		});
	}else{
		var pageObject = locator.split("-");
		browser.element(pageObject[0], pageObject[1], function (obj) {
			browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
				if (object_displayed_status.state == 'success') callback(true);
				else callback(false);
			});
		});
	}
};
*/

var elementDisplayedStatus = function (locator, callback) {
	var pageObject = locator.split("|");
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var status;
	
	if (locateStrategy == 'xpath') locateStrategy = 'xpath';
	else locateStrategy = 'css selector';
	
	browser.element(locateStrategy, object, function (obj) {
		browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
			console.log(object_displayed_status.status);
			if (object_displayed_status.status == 0) status = true;
			else status = false;
			callback(status);
		});
	});
};

var isNotPresent = function (locator, callback) {
	browser.pause(data.averagepause);
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@' + pageAndObject[1];
	page[mpage].waitForElementNotPresent(object, data.averagepause, function () {
		callback()
	})
};

var getText = function (locator, callback) {
	if (locator.includes("|")) {
		var pageAndObject = locator.split("|");
		var mpage = pageAndObject[0];
		var object = '@' + pageAndObject[1];
		page[mpage].getText(object, function (result) {
			return callback(result.value)
		});
	} else {
		browser.useXpath().getText(locator, function (result) {
			return callback(result.value)
		});
	}
};

var setText = function (locator, content) {
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@' + pageAndObject[1];
	var value = content.toString();
	page[mpage].clearValue(object);
	page[mpage].setValue(object, value);
};

var fnRandomEmailId = function (callback) {
	var max = 9, min = 0, str="", i;
	for(i=0; i < 5; i++){
		str = str + Math.floor(Math.random() * (max - min) + min);
	}
	return callback('email'+ str +'@mercer.com');
};

var fnRandomPhoneNumber = function (callback) {
	var max = 9, min = 0, str="", i;
	for(i=0; i < 10; i++){
		str = str + Math.floor(Math.random() * (max - min) + min);
	}
	return callback(str);
};

var wait_a_Second = function (callback) {
	browser.pause(data.averagepause, function () {
		callback();
	})
};

var wait_a_bit_long = function (callback) {
	browser.pause(data.shortwaittime, function () {
		callback();
	})
};

var verifyText = function (locator, value) {
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@' + pageAndObject[1];
	isDisplayed(locator, function () {
		page[mpage].getText(object, function (response) {
			console.log("Excepted: " + value);
			console.log("Actual: " + response.value);
			browser.assert.equal(response.value, value)
			if (response.value != value) {
				console.log("Text Content mismatch: ");
			}
		});
	});
};

var verifyInputBoxText = function (locator, value) {
	getInputBoxText(locator, function (inputBoxText) {
		console.log("Excepted: " + inputBoxText.toUpperCase());
		console.log("Actual: " + value.toUpperCase());
		browser.assert.equal(inputBoxText.toUpperCase(), value.toUpperCase())
		if (inputBoxText.toUpperCase() != value.toUpperCase()) {
			console.log("Text Content mismatch: ");
		}
	});
};

var verifyValuesEqual = function (msg, value1, value2) {
	console.log(msg);
	console.log('Expected: ' + value1);
	console.log('  Actual: ' + value2);
	browser.verify.equal(value1, value2);
};

var performClick = function (locator, callback) {
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@'+pageAndObject[1];
	scroll(locator, function () {
		page[mpage].waitForElementPresent(object,20000);
		page[mpage].click(object);
		browser.waitForElementPresent('body',data.averagepause);
		//browser.pause(data.shortpause);
		callback();
	});
};

var fnRandomInteger = function (min, max, callback) {
	var random = min + Math.floor(Math.random() * (max - min + 1));
	return callback(random);
};

var selectWithVisibleText = function (locator, value, callback) {
	var pageObject = locator.split("|");
	var objLocation = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var options_location, count;
	
	if (locateStrategy == 'xpath') {
		options_location = objLocation + '//option';
	} else {
		locateStrategy = 'css selector';
		options_location = objLocation + ' > option';
	}
	
	page[[pageObject[0]]].click('@' + pageObject[1]);
	
	browser.elements(locateStrategy, options_location, function (webElementsArray) {
		count = webElementsArray.value.length;
		webElementsArray.value.forEach(function (webEle) {
			browser.elementIdText(webEle.ELEMENT, function (ele) {
				if (ele.value == value) {
					browser.elementIdClick(webEle.ELEMENT)
					browser.pause(data.averagepause);
					page[[pageObject[0]]].click('@' + pageObject[1]);
					callback();
				}
			})
		});
	});
	
};

var selectRandomly = function (locator, callback) {
	var pageObject = locator.split("|");
	var objLocation = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var options_location, count, i = 0;
	
	if (locateStrategy == 'xpath') {
		options_location = objLocation + '//option';
	} else {
		locateStrategy = 'css selector';
		options_location = objLocation + ' > option';
	}
	
	getInputBoxText(locator, function (selected_value) {
		page[[pageObject[0]]].click('@' + pageObject[1]);
		browser.elements(locateStrategy, options_location, function (webElementsArray) {
			count = parseInt(webElementsArray.value.length) - 1;
			fnRandomInteger(0, count, function (index) {
				webElementsArray.value.forEach(function (webEle) {
					browser.elementIdText(webEle.ELEMENT, function (ele) {
						if (i == parseInt(index)) {
							if (ele.value == selected_value) {
								selectRandomly(locator, function (data) {
									page[[pageObject[0]]].click('@' + pageObject[1]);
									callback(data);
								});
							} else {
								browser.elementIdClick(webEle.ELEMENT);
								browser.pause(data.shortpause);
								page[[pageObject[0]]].click('@' + pageObject[1]);
								callback(ele.value);
							}
						}
						i++;
					})
				});
			});
		});
	});
};

var selectDropdownUI = function(locator, value, callback) {
	var pageObject = locator.split("|");
	var mpage = pageObject[0];
	var object = '@' + pageObject[1];
	
	var options = Objects.sections[mpage].elements[pageObject[1]].selector;
	options = options + '//option[starts-with(text(),"' + value + '")]';
	page[mpage].click(object);
	browser.pause(data.shortpause);
	getText(options, function (selectedOptions) {
		browser.useXpath().waitForElementVisible(options, data.averagepause);
		browser.useXpath().click(options);
		browser.pause(data.shortpause);
		browser.useXpath().click(options);
		browser.pause(data.shortpause);
		browser.keys(browser.Keys.ENTER);
		browser.pause(data.shortpause);
		//callback(selectedOptions)
	});
};

var selectDropdown = function (locator, value) {
	var pageObject = locator.split("|");
	var identifier = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	browser.execute(function (identifier, value) {
		var objSelect = document.getElementById(identifier);
		setSelectedValue(objSelect, value);
		function setSelectedValue(selectObj, valueToSet) {
			for (var i = 0; i < selectObj.options.length; i++) {
				if (selectObj.options[i].text== valueToSet) {
					selectObj.options[i].selected = true;
					return;
				}
			}
		}
	}, [identifier, value], function (result) {});
};

var readListData = function (locator, callback) {
	var data = "", count = 0;
	var pageObject = locator.split("|");
	var mpage = pageObject[0];
	var object = pageObject[1];
	browser.elements('xpath', Objects.sections[mpage].elements[object].selector, function (webElementsArray) {
		count = webElementsArray.value.length;
		webElementsArray.value.forEach(function (webEle) {
			browser.elementIdText(webEle.ELEMENT, function (result) {
				count--;
				data = data + '-' + result.value
				if (count <= 0) {
					data = data.substring(1, data.length);
					callback(data);
				}
			});
		});
	});
};
var downloadListData = function (locator, callback) {
	var data = "", count = 0;
	var pageObject = locator.split("|");
	var mpage = pageObject[0];
	var object = pageObject[1];
	browser.elements('xpath', Objects.sections[mpage].elements[object].selector, function (webElementsArray) {
		count = 2;
		
		webElementsArray.value.forEach(function (webEle) {
			browser.elementIdText(webEle.ELEMENT, function (ele) {
				count--;
				if (count) {
					browser.elementIdClick(webEle.ELEMENT);
					browser.pause(5000)
				}
			})
		});
	});
};

var getWebElementsCount = function (locator, callback) {
	var count = 0;
	if (locator.includes("|")) {
		var pageObject = locator.split("|");
		var mpage = pageObject[0];
		var object = pageObject[1];
		browser.elements('xpath', Objects.sections[mpage].elements[object].selector, function (webElementsArray) {
			count = webElementsArray.value.length;
			callback(count);
		});
	} else {
		browser.elements('xpath', locator, function (webElementsArray) {
			count = webElementsArray.value.length;
			callback(count);
		});
	}
	
};

var readListDataOnlyDisplayedValues = function (locator, callback) {
	var data = "", count = 0;
	browser.elements('xpath', locator, function (webElementsArray) {
		count = webElementsArray.value.length;
		webElementsArray.value.forEach(function (webEle) {
			browser.elementIdText(webEle.ELEMENT, function (result) {
				count--;
				if (result.value != '' && result.value != undefined)
					data = data + '-' + result.value
				if (count <= 0) {
					data = data.substring(1, data.length);
					callback(data);
				}
			});
		});
	});
};

var dragAndDropOverAnotherElement = function (webElement1, webElement2, callback) {
	var pageObject = webElement1.split("|");
	var mpage = pageObject[0];
	var Object1 = Objects.sections[mpage].elements[pageObject[1]].selector;
	
	var pageObject = webElement2.split("|");
	var mpage = pageObject[0];
	var Object2 = Objects.sections[mpage].elements[pageObject[1]].selector;
	
	browser.useXpath()
	browser.moveToElement(Object1, 0, 0)
	browser.mouseButtonDown(0)
	browser.moveToElement(Object2, 0, 0)
	browser.mouseButtonUp(0)
	browser.pause(data.shortpause);
	callback();
};

var getInputBoxText = function (locator, callback) {
	var pageAndObject = locator.split("|")
	var mpage = pageAndObject[0]
	var object = '@' + pageAndObject[1]
	page[mpage].getValue(object, function (result) {
		callback(result.value);
	})
};

var FocusOnFileNameInput = function (callback) {
	var Keyboard = robot.Keyboard;
	var k = Keyboard();
	var tabIndex = [];
	var times = 4;
	for (var i = 0; i < times; i++) {
		tabIndex.push(i + 1);
	}
	browser.pause(data.shortpause);
	k.click(robot.KEY_F3);
	k.click(robot.KEY_ENTER);
	browser.pause(data.shortpause);
	tabIndex.forEach(function (index) {
		setTimeout(function () {
			k.click(robot.KEY_TAB);
			browser.pause(data.shortpause);
			if (index == times) {
				k.click(robot.KEY_ENTER);
				browser.pause(data.averagepause);
				callback();
			}
		}, 2000);
		
	});
};
var RFocusOnFileNameInput = function (callback) {
	//var Keyboard = robot.Keyboard;
	var k = robot123;
	var tabIndex = [];
	var times = 4;
	for (var i = 0; i < times; i++) {
		tabIndex.push(i + 1);
	}
	browser.pause(data.shortpause);
	k.keyTap("f3");
	browser.pause(data.shortpause);
	k.keyTap("enter");
	browser.pause(data.shortpause);
	tabIndex.forEach(function (index) {
		setTimeout(function () {
			k.keyTap("tab");
			browser.pause(data.shortpause);
			if (index == times) {
				k.keyTap("enter");
				browser.pause(data.averagepause);
				callback();
			}
		}, 2000);
		
	});
};

var pastePath = function (callback) {
	//var Keyboard = robot.Keyboard;
	//var k = Keyboard();
	/*k.press(robot.KEY_CONTROL);
	browser.pause(data.shortpause);
	k.click(robot.KEY_V);
	browser.pause(data.shortpause);
	k.release(robot.KEY_CONTROL);
	browser.pause(data.shortpause);
	k.click(robot.KEY_ENTER);
	browser.pause(data.shortpause);*/
	var k=robot123;
	k.keyToggle("control","down");
	browser.pause(data.shortpause);
	k.keyTap("v");
	browser.pause(data.shortpause);
	k.keyToggle("control","up");
	browser.pause(data.shortpause);
	k.keyTap("enter");
	browser.pause(data.shortpause);
	
	callback();
};

var NavigateToFolderLocation = function (callback) {
	browser.pause(data.shortpause);
	FocusOnTopFolderNavigator(function () {
		browser.pause(data.shortpause);
		pastePath(function () {
			callback();
		})
	})
};

var FocusOnTopFolderNavigator = function (callback) {
	/*var Keyboard = robot.Keyboard;
	var k = Keyboard();*/
	var k = robot123;
	setTimeout(function () {
		k.keyTap("f4");
		browser.pause(data.shortpause);
		k.keyToggle("control","down");
		browser.pause(data.shortpause);
		k.keyTap("a");
		browser.pause(data.shortpause);
		k.keyToggle("control","up");
		browser.pause(data.shortpause);
		k.keyTap("backspace");
		browser.pause(data.shortpause);
		callback();
		
		
		/*k.click(robot.KEY_F4);
		browser.pause(data.shortpause);
		k.press(robot.KEY_CONTROL);
		browser.pause(data.shortpause);
		k.click(robot.KEY_A);
		browser.pause(data.shortpause);
		k.release(robot.KEY_CONTROL);
		browser.pause(data.shortpause);
		k.click(robot.KEY_BACKSPACE);
		browser.pause(data.shortpause);*/
		
	}, 4000);
};

var TypeFileName = function (filename, callback) {
	//var Keyboard = robot.Keyboard;
	var k = robot123;
	var keystoke = [];
	FocusOnFileNameInput(function () {
		browser.pause(data.shortpause);
		for (var i = 0, len = filename.length; i < len; i++) {
			if (filename[i] == '/') {
				keystoke.push("/");
			} else if (filename[i] == '-') {
				keystoke.push("-");
			} else {
				keystoke.push(filename[i].toUpperCase());
			}
		}
		var count = keystoke.length;
		keystoke.forEach(function (key) {
		//	k.click(robot[key]);
			k.typeString(key);
			count = count - 1;
			if (count < 1) {
				//k.click(robot.KEY_ENTER);
				k.keyTap('enter')
				callback();
			}
		});
	})
};

var UploadFile = function (file, callback) {
        file = file.replace('\\', '/');
        var slashIndex = file.lastIndexOf('/');
        var path = file.substring(0, slashIndex);
        var filename = file.substring(parseInt(slashIndex) + 1, file.length);
        clipboard.copy(path, function () {
            browser.pause(data.shortpause);
            NavigateToFolderLocation(function () {
                browser.pause(data.shortpause);
                TypeFileName(filename, function () {
                    browser.pause(data.shortpause);
                    callback('File uploading Completed!!');
                });
            });
        });
};

var fnRandomDate = function (callback) {
	fnRandomInteger(1, 30, function (day) {
		fnRandomInteger(3, 12, function (month) {
			fnRandomInteger(1999, 2014, function (year) {
				if (parseInt(day) < 10) {
					day = '0' + day;
				}
				if (parseInt(month) < 10) {
					month = '0' + month;
				}
				var date = day + "/" + month + "/" + year;
				callback(date);
			})
		})
	})
};

var splitAndSort = function (input, callback) {
	var arr = input.split("-");
	arr = arr.sort();
	var output = "", count = 0;
	arr.forEach(function (each) {
		output = output + "-" + each;
		count++;
		if (count == arr.length) {
			output = output.replace("--", "-")
			output = output.substring(1, output.length);
			callback(output);
		}
	});
};

var getCheckboxState = function (locator, callback) {
	var pageObject = locator.split("|");
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var status;
	
	if (locateStrategy == 'xpath') locateStrategy = 'xpath';
	else locateStrategy = 'css selector';
	
	browser.element(locateStrategy, object, function (obj) {
		browser.elementIdSelected(obj.value.ELEMENT, function (object_selected_status) {
			callback(object_selected_status.value);
		});
	});
};

var isElementEnabled = function (locator, callback) {
	var pageObject = locator.split("|");
	var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
	var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
	var status;
	
	if (locateStrategy == 'xpath') locateStrategy = 'xpath';
	else locateStrategy = 'css selector';
	
	browser.element(locateStrategy, object, function (obj) {
		browser.elementIdEnabled(obj.value.ELEMENT, function (object_selected_status) {
			callback(object_selected_status.value);
		});
	});
};

var verifyTextNotEqual = function (locator, value) {
	var pageAndObject = locator.split("|");
	var mpage = pageAndObject[0];
	var object = '@' + pageAndObject[1];
	isDisplayed(locator, function () {
		page[mpage].getText(object, function (response) {
			console.log("Excepted: " + value);
			console.log("Actual: " + response.value);
			browser.assert.notEqual(response.value, value)
			if (response.value = value) {
				console.log("Text Content match: ");
			}
		});
	});
};

var getTodaysDate = function (format, callback) {
	var day = new Date().getDate();
	var month = new Date().getMonth();
	var year = new Date().getFullYear();
	var monthName;
	
	switch (parseInt(month) + 1) {
		case 1:
			monthName = "January";
			break;
		case 2:
			monthName = "Febuary";
			break;
		case 3:
			monthName = "March";
			break;
		case 4:
			monthName = "April";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "June";
			break;
		case 7:
			monthName = "July";
			break;
		case 8:
			monthName = "August";
			break;
		case 9:
			monthName = "September";
			break;
		case 10:
			monthName = "October";
			break;
		case 11:
			monthName = "November";
			break;
		case 12:
			monthName = "December";
			break;
	}
	if (format == 'DD/MM/YYYY') {
		month = (parseInt(month) + 1);
		if (parseInt(day) < 10) day = '0' + parseInt(day);
		if (parseInt(month) < 10) month = '0' + parseInt(month);
		callback(day + '/' + month + '/' + year);
	}
	else if(format == 'MM DD, YYYY'){
		if (parseInt(day) < 10) day = '0' + parseInt(day);
		callback(monthName.substring(0, 3) + ' ' + day + ', ' + year);
	}else {
		callback(day + ' ' + monthName + ', ' + year);
	}
};

var getLoggedUserEmail = function (callback) {
	callback(user_email.toLowerCase());
};
var AcceptingPopup = function(){
	var Keyboard = robot.Keyboard;
	var k = Keyboard();
	//browser.pause(data.shortpause);
	k.click(robot.KEY_ENTER);
	//browser.pause(data.shortpause);
};
var RejectingPopup = function(){
	var Keyboard = robot.Keyboard;
	var k = Keyboard();
	k.click(robot.KEY_TAB);
	k.click(robot.KEY_ENTER);
};

var waitTillElementLoads = function (locator,callback) {
	browser.pause(data.shortWait);
	elementDisplayedStatus(locator, function (displayStatus) {
		if(displayStatus){
			browser.pause(data.shortWait);
			callback(true);
		}else{
			waitTillElementLoads(locator, callback);
		}
	})
};
var CompareExcelData = function (config) {
	
	var mkdirp=require('mkdirp');
	var file1_obj = xlsx.parse(config.file1); // parses a file
	var file2_obj = xlsx.parse(config.file2);
	var file1_itemId = [];
	var file2_itemId = [];
	var filter_item_arr = [];
	var fname;
	var excel_name1=[],excel_name2=[];
	excel_name1=config.file1.split('/');
	excel_name2=config.file2.split('/');
	console.log(excel_name1[4]);
	console.log(excel_name2[4]);
	fname= "CompareExcel_"+new Date().getMonth()+new Date().getFullYear()+"_"+new Date().getTime();
	mkdirp(homepath +"/Downloads/ESR/"+fname, function (err)
	{
		if (err) {console.error("err is "+err);}
		else {
			console.log('Dir created!');
			_.each(file1_obj, function (i) {
				_.each(file2_obj, function (j) {
					if(i.name == j.name)
					{
						filter_item_arr.length=0;
						console.log("Name of the sheet: "+i.name);
						_.each(i.data, function (item) {
							var file1_itemValArr = [];
							file1_itemValArr.push(item);
							var joinVal = file1_itemValArr.join('');
							file1_itemId.push(joinVal)
						});
						
						_.each(j.data, function (item) {
							var file2_itemValArr = [];
							file2_itemValArr.push(item);
							var joinVal = file2_itemValArr.join('');
							file2_itemId.push(joinVal)
						});
						
						_.each(file1_itemId, function (file1_item, index) {
							_.each(file2_itemId, function (file2_item) {
								if (file1_item === file2_item) {
									delete file1_itemId[index]
								}
							})
						});
						
						_.each(file1_itemId, function (filter_item,index) {
							if (filter_item !== '' && filter_item !== undefined) {
								filter_item_arr.push("SheetName: "+i.name+" Row number: "+index+" "+filter_item);
							}
						});
						_.each(filter_item_arr, function (d,index) {
							
							if(index == filter_item_arr.length-1)
							{
								exdata(d);
							}
							
						});
						function exdata(dataa){
							writeFile(dataa,homepath +"/Downloads/ESR/ESR/"+i.name+".txt",function(){
								console.log("Written!!")
							});
						}
						/*function exdata(dataa){
							writeFile(dataa,homepath +"/Downloads/ESR/"+fname+"/"+i.name+".txt",function(){
								/!*move(config.file1,homepath +"/Downloads/ESR/"+fname+"/"+excel_name1[4],function () {
									move(config.file2,homepath +"/Downloads/"+fname+"/"+excel_name2[4],function () {
										console.log("Moved the files successfully!!")
									});
								});*!/
							});
						}*/
					}
				})
			});
			
		}
		return filter_item_arr;
	});
	
	
	function writeFile(content,filename,callback){
		fs.writeFile(filename,content,function(err){
			if(err){
				throw 'error writing file:'+err;
				callback();
			}
			console.log('wrote successfully');
			callback();
		})
	}
	function move(oldPath, newPath, callback) {
		
		fs.rename(oldPath, newPath, function (err) {
			if (err) {
				if (err.code === 'EXDEV') {
					copy();
				} else {
					callback(err);
				}
				return;
			}
			callback();
		});
		
		function copy() {
			var readStream = fs.createReadStream(oldPath);
			var writeStream = fs.createWriteStream(newPath);
			
			readStream.on('error', callback);
			writeStream.on('error', callback);
			
			readStream.on('close', function () {
				fs.unlink(oldPath, callback);
			});
			
			readStream.pipe(writeStream);
		}
	}
	
	
};
var CompareExcelMonthlyData = function (format, callback) {
	var path = osHomedir();
	var glob = require("glob")
	var fs=require('fs');
	var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
	//console.log(homepath);
	
	glob(homepath +"/Downloads/*.xlsx",  function (er, files) {
		//console.log(files);
		if(er)
		{
			console.log("There are no excel files");
		}
		var Excel = require('exceljs');
		var location_excel1 = files[0];
		var location_excel2 = files[1];
		var timeNow = new Date().getDate();
		var fn=format+"_"+timeNow+new Date().getMonth()+new Date().getFullYear()+"_"+new Date().getTime();
		//console.log(location_excel1);
		//console.log(location_excel2);
		var excel_name1=[],excel_name2=[];
		excel_name1=location_excel1.split('/');
		excel_name2=location_excel2.split('/');
		console.log(excel_name1[4]);
		console.log(excel_name2[4]);
		readExcel(location_excel1, function (excel_content1) {
			readExcel(location_excel2, function (excel_content2) {
				
				compareExcel(excel_content1, excel_content2, function (output) {
				    writeFile(output,homepath +"/Downloads/ESR_Automation_Reports/"+fn+".txt",function () {
						console.log(output);
				    	move(location_excel1,homepath +"/Downloads/ESR_Automation_Reports/"+excel_name1[4],function () {
							move(location_excel2,homepath +"/Downloads/ESR_Automation_Reports/"+excel_name2[4],function () {
								console.log("Moved the files successfully!!")
							});
						});
						
					});
					
				});
			});
		});
		
		function deleteFiles(file,callback) {
			fs.unlinkSync(file);
			callback();
		}
		function writeFile(content,filename,callback){
			fs.writeFile(filename,content,function(err){
				if(err){
					throw 'error writing file:'+err;
					callback();
				}
				console.log('wrote successfully');
				callback();
			})
		}
		function readExcel(location, callback){
			var workbook = new Excel.Workbook();
			var cell_value="";
			var data_array = [];
			var i=0;
			workbook.xlsx.readFile(location)
				.then(function() {
					var worksheet = workbook.getWorksheet(1);
					
					worksheet.eachRow({ includeEmpty: true }, function(row) {
						
						i++;
						row.eachCell({ includeEmpty: true }, function(cell) {
							
							// console.log(JSON.stringify(cell.value));
							cell_value += cell.value.toString().replace(/\/r/g,"").replace(/\/n/g,"") + "|";
						});
						 // console.log('value of i: '+ i);
						//console.log('value of rowcount: '+ worksheet.rowCount);
						data_array.push(cell_value);
						cell_value = "";
						if(i==worksheet.rowCount-1){
							console.log(i);
							callback(data_array);
						}
					});
				});
		}
		function compareExcel(v1, v2, callback){
			var flag = true;
			var mismatch_row = [];
			var mismatch_data=[];
			//console.log("Excel Data 1 :       "+v1);
			//console.log("Excel Data 2 :       "+v2);
			
			for(var i=0; i< v2.length; i++){
				if(v1[i]!=v2[i]){
					mismatch_row.push(i+1+"\n");
					
					mismatch_data.push("\ndata 1 : "+v1[i]+"\ndata 2 : "+v2[i]);
					
					flag = false;
				}
				if(i==v2.length-1) {
					if(flag) {
						console.log("Same data");
						callback("***********Both excel contain same data!!!************");
					}
					else{
						callback("~ There is mismatch in row number : "+ mismatch_row + "\n" + mismatch_data);
						console.log("Mismatch");
						
					}
				}
			}
		}
		function move(oldPath, newPath, callback) {
			
			fs.rename(oldPath, newPath, function (err) {
				if (err) {
					if (err.code === 'EXDEV') {
						copy();
					} else {
						callback(err);
					}
					return;
				}
				callback();
			});
			
			function copy() {
				var readStream = fs.createReadStream(oldPath);
				var writeStream = fs.createWriteStream(newPath);
				
				readStream.on('error', callback);
				writeStream.on('error', callback);
				
				readStream.on('close', function () {
					fs.unlink(oldPath, callback);
				});
				
				readStream.pipe(writeStream);
			}
		}
	});
	
	
	
	
};
var CompareExcelAnnualData = function (format,SheetNo, callback) {
	var path = osHomedir();
	var glob = require("glob")
	var fs=require('fs');
	var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
	//console.log(homepath);
	
	glob(homepath +"/Downloads/*.xlsx",  function (er, files) {
		//console.log(files);
		if(er)
		{
			console.log("There are no excel files");
		}
		var Excel = require('exceljs');
		var location_excel1 = files[0];
		var location_excel2 = files[1];
		var timeNow = new Date().getDate();
		var fn=format+"_"+timeNow+new Date().getMonth()+new Date().getFullYear()+"_"+new Date().getTime();
		//console.log(location_excel1);
		//console.log(location_excel2);
		var excel_name1=[],excel_name2=[];
		excel_name1=location_excel1.split('/');
		excel_name2=location_excel2.split('/');
		console.log(excel_name1[4]);
		console.log(excel_name2[4]);
		readExcel(location_excel1, function (excel_content1) {
			readExcel(location_excel2, function (excel_content2) {
				
				compareExcel(excel_content1, excel_content2, function (output) {
					writeFile(output,homepath +"/Downloads/ESR_Automation_Reports/"+fn+".txt",function () {
						console.log(output);
						move(location_excel1,homepath +"/Downloads/ESR_Automation_Reports/"+excel_name1[4],function () {
							move(location_excel2,homepath +"/Downloads/ESR_Automation_Reports/"+excel_name2[4],function () {
								console.log("Moved the files successfully!!")
							});
						});
						
					});
					
				});
			});
		});
		
		function deleteFiles(file,callback) {
			fs.unlinkSync(file);
			callback();
		}
		function writeFile(content,filename,callback){
			fs.writeFile(filename,content,function(err){
				if(err){
					throw 'error writing file:'+err;
					callback();
				}
				console.log('wrote successfully');
				callback();
			})
		}
		function readExcel(location, callback){
			var workbook = new Excel.Workbook();
			var cell_value="";
			var data_array = [];
			var i=0;
			workbook.xlsx.readFile(location)
				.then(function() {
					var worksheet = workbook.getWorksheet(SheetNo);
					
					worksheet.eachRow({ includeEmpty: true }, function(row) {
						
						i++;
						row.eachCell({ includeEmpty: true }, function(cell) {
							
							// console.log(JSON.stringify(cell.value));
							cell_value += cell.value.toString().replace(/\/r/g,"").replace(/\/n/g,"") + "|";
						});
						// console.log('value of i: '+ i);
						//console.log('value of rowcount: '+ worksheet.rowCount);
						data_array.push(cell_value);
						cell_value = "";
						if(i==worksheet.rowCount-1){
							console.log(i);
							callback(data_array);
						}
					});
				});
		}
		function compareExcel(v1, v2, callback){
			var flag = true;
			var mismatch_row = [];
			var mismatch_data=[];
			//console.log("Excel Data 1 :       "+v1);
			//console.log("Excel Data 2 :       "+v2);
			
			for(var i=0; i< v2.length; i++){
				if(v1[i]!=v2[i]){
					mismatch_row.push(i+1+"\n");
					
					mismatch_data.push("\ndata 1 : "+v1[i]+"\ndata 2 : "+v2[i]);
					
					flag = false;
				}
				if(i==v2.length-1) {
					if(flag) {
						console.log("Same data");
						callback("***********Both excel contain same data!!!************");
					}
					else{
						callback("~ There is mismatch in row number : "+ mismatch_row + "\n" + mismatch_data);
						console.log("Mismatch");
						
					}
				}
			}
		}
		function move(oldPath, newPath, callback) {
			
			fs.rename(oldPath, newPath, function (err) {
				if (err) {
					if (err.code === 'EXDEV') {
						copy();
					} else {
						callback(err);
					}
					return;
				}
				callback();
			});
			
			function copy() {
				var readStream = fs.createReadStream(oldPath);
				var writeStream = fs.createWriteStream(newPath);
				
				readStream.on('error', callback);
				writeStream.on('error', callback);
				
				readStream.on('close', function () {
					fs.unlink(oldPath, callback);
				});
				
				readStream.pipe(writeStream);
			}
		}
	});
	
	
	
	
};

module.exports.initializePageObjects=initializePageObjects;
module.exports.CompareExcelMonthlyData=CompareExcelMonthlyData;
module.exports.CompareExcelAnnualData=CompareExcelAnnualData;
module.exports.CompareExcelData=CompareExcelData;
module.exports.getCheckboxState = getCheckboxState;
module.exports.isElementEnabled = isElementEnabled;
module.exports.verifyValuesEqual = verifyValuesEqual;
module.exports.splitAndSort = splitAndSort;
module.exports.fnRandomDate = fnRandomDate;
module.exports.performClick = performClick;
module.exports.getText = getText;
module.exports.setText = setText;
module.exports.isDisplayed = isDisplayed;
module.exports.isNotDisplayed = isNotDisplayed;
module.exports.verifyText = verifyText;
module.exports.wait_a_Second = wait_a_Second;
module.exports.wait_a_bit_long = wait_a_bit_long;
module.exports.fnRandomPhoneNumber = fnRandomPhoneNumber;
module.exports.fnRandomEmailId = fnRandomEmailId;
module.exports.verifyAlert = verifyAlert;
module.exports.fnRandomInteger = fnRandomInteger;
module.exports.readListData = readListData;
module.exports.dragAndDropOverAnotherElement = dragAndDropOverAnotherElement;
module.exports.getInputBoxText = getInputBoxText;
module.exports.readListDataOnlyDisplayedValues = readListDataOnlyDisplayedValues;
module.exports.UploadFile = UploadFile;
module.exports.getWebElementsCount = getWebElementsCount;
module.exports.verifyInputBoxText = verifyInputBoxText;
module.exports.isNotPresent = isNotPresent;
module.exports.elementDisplayedStatus = elementDisplayedStatus;
module.exports.selectWithVisibleText = selectWithVisibleText;
module.exports.selectRandomly = selectRandomly;
module.exports.selectDropdownUI = selectDropdownUI;
module.exports.selectDropdown = selectDropdown;
module.exports.getLoggedUserEmail = getLoggedUserEmail;
module.exports.verifyTextNotEqual = verifyTextNotEqual;
module.exports.getTodaysDate = getTodaysDate;
module.exports.waitTillElementLoads = waitTillElementLoads;
module.exports.downloadListData = downloadListData;