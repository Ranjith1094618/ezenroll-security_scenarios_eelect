//console.log(process.env.TEST_ENV);
module.exports = {
    BrowserInTest: 'chrome',
    //TestingEnvironment: process.env.TEST_ENV,
    TestingEnvironment: 'Test1',
    shortWait: 2000,
    longWait: 20000,


    urls: {
        AUTO_BENE: {
            Test1: {
                vaildUrl: 'https://test1.autobene.com/',
                invalidUrl: 'http://test1.autobene.com'
            },

            QA: {
                vaildUrl: 'https://test2.autobene.com/',
                invalidUrl: 'http://test2.autobene.com'

            },
            Staging: {
                vaildUrl: 'https://test3.autobene.com/',
                invalidUrl: 'http://test3.autobene.com'
            },
            Prod: {
                vaildUrl: 'https://www.autobene.com/',
                invalidUrl: 'http://www.autobene.com'

            }
        },
        eElect: {
            Test1: {
                vaildUrl: 'https://test.eelect.com/',
                invalidUrl: 'http://test.eelect.com/'
            },

            QA: {
                vaildUrl: 'https://test2.eelect.com/',
                invalidUrl: 'http://test2.eelect.com/'

            },
            Staging: {
                vaildUrl: 'https://test3.eelect.com/',
                invalidUrl: 'http://test3.eelect.com/'
            },
            Prod: {
                vaildUrl: 'https://www.eelect.com/',
                invalidUrl: 'http://www.eelect.com/'

            }

        }

    },
    users: {
        AUTO_BENE: {
            Test1: {
                username: 'Automation6834',
                password: 'Welcome5',
                ApplicationID: '91844',
                Es_AppID: '91900',
                EID: '123456',
                PIN: '1111'
            },
            QA: {
                username: 'Automation6834',
                password: 'Welcome5',

            },
            Staging: {
                username: 'Automation6834',
                password: 'Welcome5'

            },
            Prod: {
                username: 'Automation6834',
                password: 'Welcome5'

            }
        },
        eElect: {
            Test1: {
                username: 'Username01',
                password: 'Password01',
                ApplicationID: '91823',
                invalidUsername: 'Username02',
                invalidpassword: 'password02',
                Es_AppID: "91844",
                EID: '11111',
                PIN: '1111'
            },
            QA: {
                username: 'Username01',
                password: 'Password01',
                invalidUsername: 'Username02',
                invalidpassword: 'password02',
                ApplicationID: '91404'
            },
            Staging: {
                username: 'Username01',
                password: 'Password01',
                invalidUsername: 'Username02',
                invalidpassword: 'password02',
                ApplicationID: '96231'

            },
            Prod: {
                username: 'Username01',
                password: 'Password01',
                invalidUsername: 'Username02',
                invalidpassword: 'password02',
                ApplicationID: '96188'

            }
        }
    },
    SectionData: {}
};