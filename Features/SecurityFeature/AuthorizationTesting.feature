Feature: Authorization Testing

  @eElect
  Scenario: Testing for Privilege escalation
    Given Employee opens eElect Non ESApplication in Browser
    When  Login with EID and PIN
    Then  Employee try to edit eElect Information


    @AutoBene
    Scenario: Testing for Bypassing Authorization Schema
     Given Employee opens AutoBene application in browser
      When Employee enters login details in Login Page
     When  Employee enters AppID
     And   Employee selects BeneDetails option for Application used for in Global Parameters Page do not select eElect option
