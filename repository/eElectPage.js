module.exports = {
    sections: {
        AutoBenePage: {
            selector: 'body',
            elements: {
                AutoBeneLink: {selector: "#rightContent > h3 > a"},
                //LoginID: {locateStrategy: 'xpath',selector: "///*[@id=\"rightContent\"]/div[2]/form/fieldset/div[1]/input"},
                LoginID: {selector: '#rightContent > div.login > form > fieldset > div:nth-child(1) > input[type="text"]'},
                Pwd: {selector: "#rightContent > div.login > form > fieldset > div:nth-child(2) > input[type=\"password\"]:nth-child(2)"},
                LoginButton: {selector: "#rightContent > div.login > form > fieldset > div.submit > input[type=\"submit\"]"},
                AppIDSearch: {selector: "div.searchapp-text input[name='ShowApp']"},
                GoBtn: {selector: "#SelectApplication > div > div:nth-child(1) > button"},
                SelectApp: {
                    locateStrategy: 'xpath',
                    selector: "//*[@id=\"selectExtensionForm\"]/div[2]/div/div/button"
                },
                Change_Menu: {selector: "body > div > div.header.group > div.linksContainer > div.clear > ul > li:nth-child(5) > a"},
                GlobalParameters: {selector: "#rightContent > ul:nth-child(3) > li:nth-child(2) > a"},
                BeneRadiobtn: {selector: "#rightContent > form > table > tbody > tr:nth-child(4) > td:nth-child(1) > input[type=\"radio\"]:nth-child(4)"},
                GlobalContinueBtn: {selector: "#submit1"},
                GlobalContinueBtn2: {selector: "#submit1"},
                SecurityRequirement: {selector: "#rightContent > ul:nth-child(5) > li:nth-child(3) > a"},
                ESCheckbox: {selector: "#ckbUsePassSecurity"},
                Existtable: {selector: "#rbExisting"},
                ExitSecTab: {selector: "#sel_SECGROUP_ID > option:nth-child(14)"},
                Continue1: {selector: "#rightContent > form > table:nth-child(6) > tbody > tr > td:nth-child(1) > input[type=\"submit\"]"},
                eElectLink: {selector: "#LinkCenter > a:nth-child(3)"}
            }
        },
        eElectSection: {
            selector: 'body',
            elements: {
                //BeneDetailsinput: {selector: "div.wrapper2 input"},
                eElectContinue: {selector: "body > form > section.mul-grid.auto-inputs > div.mul-box.mul-box-rnd.mul-layout-login-ctn > div.mul-box-bdy > div > div > div > div.mul-box-bdy > fieldset > ol > li.mul-form-action > button"},
                eElect_eid: {selector: "#EId"},
                eElect_Pwd: {selector: "#PIN"},
                eElect_Signin: {locateStrategy: 'xpath', selector: "//*[@id=\"btn-create-new\"]"},
                AccSettings: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Account Settings')]"},

                    Cancelbtn: {selector: "body > div.mul-dialog-position-ctn > div > div.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix > button"},
                    Errormsg: {selector: "body > div > div.create-user-error > div > div.mul-box-bdy > p[contains(text(),'We are sorry, the Password does not match the system security for the Username you entered. Please check the Username and Password combination and enter them again. Note: After 3 failed attempts your account will be locked."},
                    Errormsg1: {
                        selector: "body > section.mul-grid > form > div.mul-box.mul-box-rnd.mul-layout-login-ctn > div.col-24.mul-box-hdr > div.login-public-error > div > div.mul-box-bdy > p"
                    },
                    eElectSearch: {locateStrategy: 'xpath', selector: "//*[@id=\'txtAccessNo\']"},
                    GoContinue: {selector: "body > form > section.mul-grid.auto-inputs > div.mul-box.mul-box-rnd.mul-layout-login-ctn > div.mul-box-bdy > div > div > div > div.mul-box-bdy > fieldset > ol > li.mul-form-action > button"},
                    Errorlockout: {selector: '#timeout-dialog > div.mul-box-hdr > h4 > div.timeout-title'},
                    LogOff: {selector: "#Log-out"},
                    ConfirmLogoff: {selector: "body > div.mul-dialog-position-ctn > div > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button.mul-btn-primary.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only > span"},
                    //AdminThis: {selector: '#ctl03_Banner1_PortalTabs > tbody > tr > td:nth-child(11) > a'},
                    LogoFile: {selector: 'ctl03$Desktopthreepanes2$ThreePanes$ctl11$SITESETTINGS_LOGO'},
                    eElect_Username: {selector: "#Username"},
                    eElect_Password: {selector: "#password"},
                    eElect_Login: {locateStrategy: 'xpath', selector: "/html/body/section[2]/form/div[2]/div[2]/div/div[1]/div/button"},
                    eElect_Expire: {locateStrategy: 'xpath', selector: "/html/body/form/section[2]/div[2]/div[1]/div/div/div[2]"},
                    Document_icon: {selector: 'body > header > div.mul-masthead-navbar-ctn > div.mul-masthead-adjust.mul-visible-large-mediaquery > nav > div.col-10.nav-right > div > ul > li > a > span'},
                    //browse_file: {selector: '#docUploadFileElem'},
                    //localComputer: {selector: '#locloc'},
                    //Upload_File: {locateStrategy: 'xpath', selector: '/html/body/form/table/tbody/tr[5]/td[1]/table/tbody/tr[1]/td[3]/input[@value=\'Upload\']'},
                }
            }
        }
    }

